#[derive(Debug)]
pub struct Error {
    message: String,
    inner: Box<Inner>,
}

#[derive(Debug)]
pub enum Inner {
    IoError(std::io::Error),
}

pub type Result<T> = ::std::result::Result<T, Error>;

impl Error {
    pub fn new<E>(message: &str, error: E) -> Self
    where
        Inner: From<E>,
    {
        Error {
            message: String::from(message),
            inner: Box::new(Inner::from(error)),
        }
    }
}

impl From<std::io::Error> for Inner {
    fn from(error: std::io::Error) -> Self {
        Inner::IoError(error)
    }
}
