struct MidiData {
    header: MidiHeaderData,
    tracks: Vec<MidiTrackData>,
}

struct MidiHeaderData {
    mthd: [u8; 4],
    length: [u8; 4],
    track_format: [u8; 2],
    track_count: [u8; 2],
    delta_time: [u8; 2],
}

struct MidiTrackData {}

struct MidiEventData {}
